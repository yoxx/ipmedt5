/**
 * Copyright: Group B, 2016
 * Authors: Merijn Freie, Pepijn van Hengel, Raoul Vos
 * 
 * Include libraries
 */
#include <virtuabotixRTC.h>
#include <LiquidCrystal.h>

/**
 * Initialize the Libraries
 */
// Init RealTimeClockObject => pins(SCLK -> 12, I/O -> 11, CE -> 10)
virtuabotixRTC RTC(12, 11, 10);

// Initialize the LCD => pins(0,1,2,3,4,5)
LiquidCrystal lcd(0, 1, 2, 3, 4, 5);

/**
 * Define constants
 */
// Define Button pins
const int button1 = 6;
const int button2 = 7;
const int button3 = 8;

// Wire-buttons
const int wire1 = A0;
const int wire2 = A1;
const int wire3 = A2;

// Define Shiftregister pins
const int shift_data = A3;
const int shift_latch = A4;
const int shift_clock = A5;

// Define Light pins
const int simon_red = 1;
const int simon_yellow = 2;
const int simon_blue = 3;
const int wire_red = 4;
const int wire_yellow = 5;
const int wire_blue = 6;

/**
 * Define globals
 */
// ClockState
int clockstate = 0;
int alarmstate = 0;

// Buttonstates
int buttonstate1 = 0;
int buttonstate2 = 0;
int buttonstate3 = 0;

// Wirestates
int wirestate1 = 0;
int wirestate2 = 0;
int wirestate3 = 0;

// Alarm times
int alarmHour = 0;
int alarmMinutes = 0;

// Wires
int history1 = 0;
// SimonSays
int history2 = 0;
// Alarm Setup
int alarm_setup = 0;

// Count input and put in array
int pwcount;
byte userInput[4];

// Simon Says combination - (Gets randomized)
byte combination[4];

// RandomNumber
int randNumber;

/**
 * Setup
 */
void setup() {
// Set Date/ Time (seconds, minutes, hours, day of the week, day of the month, month, year)
RTC.setDS1302Time(00, 51, 23, 5, 17, 6, 2016);

  // Start the LCD
  lcd.begin(16, 2);

  // Setup the buttons
  pinMode(button1, INPUT);
  pinMode(button2, INPUT);
  pinMode(button3, INPUT);

  // Setup the wires
  pinMode(wire1, INPUT);
  pinMode(wire2, INPUT);
  pinMode(wire3, INPUT);

  // Setup Shiftregister
  pinMode(shift_data, OUTPUT);
  pinMode(shift_latch, OUTPUT);
  pinMode(shift_clock, OUTPUT);

  // Setup lights
  int setup_leds[6] = {HIGH,HIGH,HIGH,HIGH,HIGH,HIGH};
  registerWriteArray(setup_leds);
  delay(1000);
  int setup_leds2[6] = {LOW,LOW,LOW,LOW,LOW,LOW};
  registerWriteArray(setup_leds2);
  delay(200);
}

/**
 * Main Loop
 */
void loop() {
  // Read the time from the RTC
  RTC.updateTime();
  // Display the Time and check the Alarmstatus
  
  switch(clockstate){
    case 0: // Normal    
      checkAlarm();
      break;
    case 3: // ALARM!
      break;
  }
  
  showTime();
  checkButtons();
}

/**
 * Alarm Functions
 */
void checkButtons() {
  // Read button input
  buttonstate1 = digitalRead(button1);
  buttonstate2 = digitalRead(button2);
  buttonstate3 = digitalRead(button3);
  // Read wire input
  wirestate1 = digitalRead(wire1);
  wirestate2 = digitalRead(wire2);
  wirestate3 = digitalRead(wire3);

  
  if (buttonstate2 == HIGH && clockstate != 3) {
    lcd.clear();
    if (clockstate == 2) {
      clockstate = 0;
    } else {
      clockstate += 1;
    }
    alarmstate = 0;
  } 
  if (buttonstate1 == HIGH && clockstate > 0 && clockstate != 3) {
    if (clockstate == 1) {
      alarmHour -= 1;
      if (alarmHour < 0) {
        alarmHour = 23;
      }
    }
    if (clockstate == 2) {
      alarmMinutes -= 1;
      if (alarmMinutes < 0) {
        alarmMinutes = 59;
      }
    }
  } 
  if (buttonstate3 == HIGH && clockstate > 0 && clockstate != 3) {
    if (clockstate == 1) {
      alarmHour += 1;
      if (alarmHour == 24) {
        alarmHour = 0;
      }
    }
    if (clockstate == 2) {
      alarmMinutes += 1;
      if (alarmMinutes == 60) {
        alarmMinutes = 0;
      }
    }
  }

  // Buttoncheck for simonsays during the alarm fase
  if (clockstate == 3) {
    if (history1 == 1 && history2 == 0) {
      simonSaysLeds();
      history2 = history2 + 1;
    }
    if (history1 == 0) {
      ledBlink();
      if (wirestate1 == LOW){
        if (randNumber == 0){
          int leds[6] = {LOW,LOW,LOW,HIGH,HIGH,HIGH};
          registerWriteArray(leds);
          history1 += 1;
          delay(2500);
        }
        else {
          lcd.clear();
          lcd.setCursor(0,1);
          lcd.print("Wrong! Try Again");
        }
      }
      
      else if (wirestate2 == LOW){
       if (randNumber == 1){
          int leds[6] = {LOW,LOW,LOW,HIGH,HIGH,HIGH};
          registerWriteArray(leds);
          history1 += 1;
          delay(2500);
        }
        else {
          lcd.clear();
          lcd.setCursor(0,1);
          lcd.print("Wrong! Try Again");
        }
      }
      
      else if (wirestate3 == LOW){
        if (randNumber == 2){
          int leds[6] = {LOW,LOW,LOW,HIGH,HIGH,HIGH};
          registerWriteArray(leds);
          history1 += 1;
          delay(2500);
        }
        else {
          lcd.clear();
          lcd.setCursor(0,1);
          lcd.print("Wrong! Try Again");
        }
      }
    }
    
    if (history2 == 1) {
      if (buttonstate1 == HIGH) {
        lcd.setCursor(0,1);
        lcd.print(1);
        userInput[pwcount] = 0;
        pwcount++;
        registerWrite(simon_blue, HIGH);
        delay(200);
        registerWrite(simon_blue, LOW);
      }
      if (buttonstate2 == HIGH) {
        lcd.setCursor(0,1);
        lcd.print(2);
        userInput[pwcount] = 1;
        pwcount++;
        registerWrite(simon_yellow, HIGH);
        delay(200);
        registerWrite(simon_yellow, LOW);
      }
      if (buttonstate3 == HIGH) {
        lcd.setCursor(0,1);
        lcd.print(3);
        userInput[pwcount] = 2;
        pwcount++;
        registerWrite(simon_red, HIGH);
        delay(200);
        registerWrite(simon_red, LOW);
      }
  
      if (pwcount == 4){
      boolean check = true;
      for(byte n = 0; n < 4; n++) {
        if(userInput[n] != combination[n]) {
          check = false;
        }
      }
  
      if (check){
        for(int i = 0; i < 3; i++){
          victory();
        }
        lcd.clear();
        lcd.setCursor(0,1);
        lcd.print("You solved it!");
        pwcount = 0;
        clockstate = 0;
        alarmstate = 1;
        alarm_setup = 0;
        history1 = 0;
        history2 = 0;
        delay(1000);
        lcd.clear();
      } else {
          for(int i = 0; i < 4; i++) {
            error();
          }
          history2 = 0;
          pwcount = 0;
          lcd.clear();
          lcd.setCursor(0,1);
          lcd.print("Try again!");
          delay(1000);
          lcd.clear();
        }
      }
    }
    
    beep(100);
  }
}

void checkAlarm(){
  if(alarmHour == RTC.hours && alarmstate == 0) {
    if(alarmMinutes == RTC.minutes){
      if (wirestate1 != HIGH || wirestate2 != HIGH || wirestate3 != HIGH) {
        lcd.clear();
        lcd.setCursor(0,1);
        lcd.print("Alarm disabled");
        delay(2000);
        lcd.clear();
      } else {
        if (alarm_setup == 0) {
          // Randomize the wire
          randomSeed(analogRead(0));
          randNumber = random(3);
    
          // Randomize the leds
          for (int i = 0; i < 4; i ++) {
            combination[i] = random(3);
          }
          alarm_setup = 1;
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("Combination Set");
          lcd.setCursor(0,1);
          lcd.print(randNumber);
          delay(1000);
        }
        clockstate = 3;
      }
    }
  }
}

void showTime() {
  // Print the time on the upperline
  switch(clockstate) {
    case 0:
      lcd.setCursor(4, 0);
      print2digits(RTC.hours);
      lcd.print(":");
      print2digits(RTC.minutes);
      lcd.print(":");
      print2digits(RTC.seconds);
      // Display Day-of-Week in the lower left corner
      lcd.setCursor(0, 1);
      displayDay(RTC.dayofweek);
    
      // Display date in the lower right corner
      lcd.setCursor(5, 1);
      lcd.print(" ");
      print2digits(RTC.dayofmonth);
      lcd.print("/");
      print2digits(RTC.month);
      lcd.print("/");
      lcd.print(RTC.year);
      delay(500);
      break;
    case 1:
      lcd.setCursor(4, 0);
      print2digits(alarmHour);
      lcd.print(":");
      print2digits(alarmMinutes);
      lcd.print(":00");

      lcd.setCursor(0,1);
      lcd.print("Set alarm hour");
      delay(200);
      break;
    case 2:
      lcd.setCursor(4, 0);
      print2digits(alarmHour);
      lcd.print(":");
      print2digits(alarmMinutes);
      lcd.print(":00");

      lcd.setCursor(0,1);
      lcd.print("Set alarm mins");
      delay(200);
      break;
    case 3:
      lcd.clear();
      lcd.setCursor(4, 0);
      print2digits(RTC.hours);
      lcd.print(":");
      print2digits(RTC.minutes);
      lcd.print(":");
      print2digits(RTC.seconds);

      lcd.setCursor(0,1);
      lcd.print("Wakey, Wakey!");
      delay(250);
      break;
  }
}

void displayDay(int day) {
  String days[8] = {"","Mon","Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
  lcd.setCursor(0,1);
  lcd.print(days[day]);
}

void print2digits(int number) {
  // Output leading zero
  if (number >= 0 && number < 10) {
    lcd.write('0');
  }
  lcd.print(number);
}

/**
 * Simon Says
 */
void simonSaysLeds(){
  for (byte i = 0; i < 4; i++){
    if (combination[i] == 0){
        registerWrite(simon_blue, HIGH);
        delay(1000);
        registerWrite(simon_blue, LOW);
        delay(500);
    }
    else if (combination[i] == 1){
        registerWrite(simon_yellow, HIGH);
        delay(1000);
        registerWrite(simon_yellow, LOW);
        delay(500);
    }
    else if (combination[i] == 2){
        registerWrite(simon_red, HIGH);
        delay(1000);
        registerWrite(simon_red, LOW);
        delay(500);
    }
    
  }
}

void error(){
  int on[6] = {HIGH,HIGH,HIGH,LOW,LOW,LOW};
  registerWriteArray(on);
  delay(200);
  int off[6] = {LOW,LOW,LOW,LOW,LOW,LOW};
  registerWriteArray(off);
  delay(200);
} 

void victory(){
  int leds[6] = {HIGH,LOW,LOW,LOW,LOW,LOW};
  registerWriteArray(leds);
  delay(100);
  int leds1[6] = {LOW,HIGH,LOW,LOW,LOW,LOW};
  registerWriteArray(leds1);
  delay(100);
  int leds2[6] = {LOW,LOW,HIGH,LOW,LOW,LOW};
  registerWriteArray(leds2);
  delay(100);
  int leds3[6] = {LOW,HIGH,LOW,LOW,LOW,LOW};
  registerWriteArray(leds3);
  delay(100);
  int leds4[6] = {LOW,LOW,LOW,LOW,LOW,LOW};
  registerWriteArray(leds4);
} 

/**
 * Wire puzzle
 */
 void beep(unsigned char delayms){
  analogWrite(9, 222);      // Almost any value can be used except 0 and 255
                           // experiment to get the best tone
  delay(delayms);          // wait for a delayms ms
  analogWrite(9, 0);       // 0 turns it off
  delay(delayms);          // wait for a delayms ms   
}  

void ledBlink(){
  if (randNumber == 0){
   registerWrite(wire_red, HIGH);
   delay(250);
   registerWrite(wire_red, LOW);
   delay(250);
  }
  else if (randNumber == 1){
   registerWrite(wire_yellow, HIGH);
   delay(250);
   registerWrite(wire_yellow, LOW);
   delay(250);
  }
  else if (randNumber == 2){
   registerWrite(wire_blue, HIGH);
   delay(250);
   registerWrite(wire_blue, LOW);
   delay(250);
  }
}
 
/**
 * Shift register functions
 */
void registerWrite(int whichPin, int whichState) {
  // the bits you want to send
  byte bitsToSend = 0;
  // turn off the output so the pins don't light up
  // while you're shifting bits:
  digitalWrite(shift_latch, LOW);
  // turn on the next highest bit in bitsToSend:
  bitWrite(bitsToSend, whichPin, whichState);
  // shift the bits out:
  shiftOut(shift_data, shift_clock, MSBFIRST, bitsToSend);
    // turn on the output so the LEDs can light up:
  digitalWrite(shift_latch, HIGH);
}

void registerWriteArray(int pins[6]){
  // the bits you want to send
  byte bitsToSend = 0;
  // turn off the output so the pins don't light up
  // while you're shifting bits:
  digitalWrite(shift_latch, LOW);
  // turn on the next highest bit in bitsToSend:
  for(int a = 0; a < 6; a++) {
    bitWrite(bitsToSend, a+1, pins[a]);
  }
  // shift the bits out:
  shiftOut(shift_data, shift_clock, MSBFIRST, bitsToSend);
    // turn on the output so the LEDs can light up:
  digitalWrite(shift_latch, HIGH);
}

