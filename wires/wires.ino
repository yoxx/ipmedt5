const int w1 = 2;
const int w2 = 3;
const int w3 = 4;



const int greenled = 11;
const int redled = 12;
const int blueled = 13;

int wirestate1 = 0;
int wirestate2 = 0;
int wirestate3 = 0;

int randNumber;
int goedOfFout = 0;

void setup() {
  // put your setup code here, to run once:
  // ledjes als output
  pinMode(greenled, OUTPUT);
  pinMode(redled, OUTPUT);
  pinMode(blueled, OUTPUT);
  
  // knoppen als input
  pinMode(w1, INPUT);
  pinMode(w2, INPUT);
  pinMode(w3, INPUT);

  pinMode(9, OUTPUT);

  randomSeed(analogRead(0));
  randNumber = random(3);

  //serial print
  Serial.begin(9600);
}

void beep(unsigned char delayms){
  analogWrite(9, 222);      // Almost any value can be used except 0 and 255
                           // experiment to get the best tone
  delay(delayms);          // wait for a delayms ms
  analogWrite(9, 0);       // 0 turns it off
  delay(delayms);          // wait for a delayms ms   
}  

void ledKnipper(){
  if (randNumber == 0){
   digitalWrite(blueled, HIGH);
   delay(250);
   digitalWrite(blueled, LOW);
   delay(250);
  }
  else if (randNumber == 1){
   digitalWrite(redled, HIGH);
   delay(250);
   digitalWrite(redled, LOW);
   delay(250);
  }
  else if (randNumber == 2){
   digitalWrite(greenled, HIGH);
   delay(250);
   digitalWrite(greenled, LOW);
   delay(250);
  }
}

void loop() {
   wirestate1 = digitalRead(w1);
   wirestate2 = digitalRead(w2);
   wirestate3 = digitalRead(w3);

   if (goedOfFout == 0){
    ledKnipper();
    beep(200); 
   }
   
  if (wirestate1 == LOW){
    if (randNumber == 0){
      digitalWrite(greenled, HIGH);
      digitalWrite(redled, HIGH);
      digitalWrite(blueled, HIGH);

      goedOfFout += 1;
    }
    else {
      Serial.println("fout");
    }
  }
  
  else if (wirestate2 == LOW){
   if (randNumber == 1){
      digitalWrite(greenled, HIGH);
      digitalWrite(redled, HIGH);
      digitalWrite(blueled, HIGH);

      goedOfFout += 1;
    }
    else {
      Serial.println("fout");
    }
  }
  
  else if (wirestate3 == LOW){
    if (randNumber == 2){
      digitalWrite(greenled, HIGH);
      digitalWrite(redled, HIGH);
      digitalWrite(blueled, HIGH);

      goedOfFout += 1;
    }
    else {
      Serial.println("fout");
    }
  }
  
}
