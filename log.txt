commit 968ff0417b8e52aaeafcc8cb86432dc7f9564ec1
Author: Yoxxius <raoul-vos@hotmail.com>
Date:   Wed Jun 29 12:44:53 2016 +0200

    fixed bug with no screen when setting time

commit cc2c8249143ab95b5673436d1e5478f0b9de27e4
Author: Yoxxius <raoul-vos@hotmail.com>
Date:   Tue Jun 28 13:57:40 2016 +0200

    added comments

commit a63c85dfcdfb42056548ed6a6990130f545809bc
Author: Yoxxius <raoul-vos@hotmail.com>
Date:   Tue Jun 28 10:49:54 2016 +0200

    refactured names

commit f6e68164f190e742584fe5e34ef704c238892b65
Author: Yoxxius <raoul-vos@hotmail.com>
Date:   Tue Jun 28 10:46:30 2016 +0200

    removed redundend earlyer versions

commit aece3e95f86a9fe3acf5611cf21b3f665e407e10
Author: Yoxxius <raoul-vos@hotmail.com>
Date:   Tue Jun 28 10:18:56 2016 +0200

    minor fixes after usability tests

commit c797bc47f77a89a3b8e0d557eab9912243d6d2ae
Author: Yoxxius <raoul-vos@hotmail.com>
Date:   Thu Jun 23 13:10:32 2016 +0200

    Seperate files

commit 19d164fdb3a5e044fbca227e961b5640a9a2044e
Author: pepijnvanhengel <pepijnvanhengel@gmail.com>
Date:   Wed Jun 22 13:14:49 2016 +0200

    beep changes & comment removal

commit 74c9e7b248859e89020b499243d8f0b7fb49382c
Author: Yoxxius <raoul-vos@hotmail.com>
Date:   Tue Jun 21 22:25:49 2016 +0200

    minor tweaks

commit 69a45a8d7164580265cc4a9f3024db6bae214fc6
Author: Yoxxius <raoul-vos@hotmail.com>
Date:   Thu Jun 16 09:09:38 2016 +0200

    made flikkering startup lights

commit 8c2c2f2a99bef07fb3921416057be88e15cf5300
Author: Yoxxius <raoul-vos@hotmail.com>
Date:   Thu Jun 16 09:06:37 2016 +0200

    Made the setup use the lights aswell

commit fc35bc7a93bfcf58bd1fb4fa26d576f9450f13a2
Author: Yoxxius <raoul-vos@hotmail.com>
Date:   Wed Jun 15 15:50:47 2016 +0200

    Clock works

commit 73a1bcb4e2d59a2115bcb1722c3814016687d99d
Author: Yoxxius <raoul-vos@hotmail.com>
Date:   Tue Jun 14 15:30:33 2016 +0200

    added simon says

commit 5c189221a39f41ec1f4eb76f284bb2f95b06aa9c
Author: Yoxxius <raoul-vos@hotmail.com>
Date:   Tue Jun 14 01:11:52 2016 +0200

    initial clock and alarm module

commit 486be96b78833458a53893b81b9aeea9c965b638
Author: Yoxxius <raoul-vos@hotmail.com>
Date:   Mon Jun 13 23:07:35 2016 +0200

    blinking led

commit a1c851269148df5f21409973335e61330fafb5bb
Author: pepijnvanhengel <pepijnvanhengel@gmail.com>
Date:   Thu Jun 9 15:10:38 2016 +0200

    simonsays & wiregame
