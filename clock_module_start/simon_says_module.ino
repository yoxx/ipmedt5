/**
 * Copyright: Group B, 2016
 * Authors: Merijn Freie, Pepijn van Hengel, Raoul Vos
 * 
 * The Simon Says module that allows us to play the button game
 */

/**
 * Function to display the simonSays combination
 */
void simonSaysLeds(){
  // Notify the user to wait for the lights to finish
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Wait for the  ->");
  lcd.setCursor(0,1);
  lcd.print("lights to finish");
  delay(500);

  // Loop trough the combination array and display the lights
  for (byte i = 0; i < 4; i++){
    // Flash the blue led
    if (combination[i] == 0){
        registerWrite(simon_blue, HIGH);
        delay(750);
        registerWrite(simon_blue, LOW);
        delay(300);
    }
    // Flash the yellow led
    else if (combination[i] == 1){
        registerWrite(simon_yellow, HIGH);
        delay(750);
        registerWrite(simon_yellow, LOW);
        delay(300);
    }
    // Flash the red led
    else if (combination[i] == 2){
        registerWrite(simon_red, HIGH);
        delay(750);
        registerWrite(simon_red, LOW);
        delay(300);
    }
    // Beep
    beep(500);
  }
}

/**
 * Beep and flash the leds to indicate the user did something wrong
 */
void error(){
  beep(200);
  int on[6] = {HIGH,HIGH,HIGH,LOW,LOW,LOW};
  registerWriteArray(on);
  beep(500);
  int off[6] = {LOW,LOW,LOW,LOW,LOW,LOW};
  registerWriteArray(off);
  beep(200);
} 

/**
 * Flash the lights in a certain order to indicate the user did it right
 */
void victory(){
  int leds[6] = {HIGH,LOW,LOW,LOW,LOW,LOW};
  registerWriteArray(leds);
  delay(100);
  int leds1[6] = {LOW,HIGH,LOW,LOW,LOW,LOW};
  registerWriteArray(leds1);
  delay(100);
  int leds2[6] = {LOW,LOW,HIGH,LOW,LOW,LOW};
  registerWriteArray(leds2);
  delay(100);
  int leds3[6] = {LOW,HIGH,LOW,LOW,LOW,LOW};
  registerWriteArray(leds3);
  delay(100);
  int leds4[6] = {LOW,LOW,LOW,LOW,LOW,LOW};
  registerWriteArray(leds4);
} 

/**
 * Check the buttons during the simon says puzzle
 */
void checkSimon(){
    // Wait for the leds to finish
    if (history2 == 1) {
      // If the blue button is pressed flash the blue led and add the button to the userinput 
      if (buttonstate1 == HIGH) {
        userInput[pwcount] = 0;
        pwcount++;
        registerWrite(simon_blue, HIGH);
        delay(100);
        registerWrite(simon_blue, LOW);
        delay(200);
      }

      // If the yellow button is pressed flash the yellow led and add the button to the userinput 
      if (buttonstate2 == HIGH) {
        userInput[pwcount] = 1;
        pwcount++;
        registerWrite(simon_yellow, HIGH);
        delay(100);
        registerWrite(simon_yellow, LOW);
        delay(200);
      }

      // If the red button is pressed flash the red led and add the button to the userinput 
      if (buttonstate3 == HIGH) {
        userInput[pwcount] = 2;
        pwcount++;
        registerWrite(simon_red, HIGH);
        delay(100);
        registerWrite(simon_red, LOW);
        delay(200);
      }

      // If the inpyt count if reached
      if (pwcount == 4){
        boolean check = true;
        // check the combination
        for(byte n = 0; n < 4; n++) {
          if(userInput[n] != combination[n]) {
            // if the combination is false
            check = false;
          }
        }

        // if the combination is correct
        if (check){
          // Display the victory lights
          for(int i = 0; i < 3; i++){
            victory();
          }
          // Notify the user that the alarm is defused
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("You solved it.");
          lcd.setCursor(0,1);
          lcd.print("Have a good day!");

          // Reset variables for the next alarm time
          pwcount = 0;
          clockstate = 0;
          alarmstate = 1;
          alarmCount = 0;
          alarm_setup = 0;
          history1 = 0;
          history2 = 0;
          delay(1000);
          lcd.clear();
        } else {
          // Display the error flash lights
          for(int i = 0; i < 4; i++) {
            error();
          }
          
          // reset the variables triggering the showing of the combination
          history2 = 0;
          pwcount = 0;
          
          // Notify the user to try again
          lcd.clear();
          lcd.setCursor(0,1);
          lcd.print("Try again!");
          delay(1000);
          lcd.clear();
        }
      }
    }

    // Add to beepcount
    beepCount += 1;
    // When 4 short beeps are heard reset the count
    if(beepCount == 4){
      beepCount = 0;
      beep(100);
    }
  }


