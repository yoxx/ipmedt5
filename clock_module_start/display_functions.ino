/**
 * Copyright: Group B, 2016
 * Authors: Merijn Freie, Pepijn van Hengel, Raoul Vos
 * 
 * The display functions used to display items on the lcd screen
 */

/**
 * Display the time for different states
 */
void showTime() {
  // Print the time on the upperline
  switch(clockstate) {
    // Default state display data/time
    case 0: 
      // Show the time in the middle of the upper row
      lcd.setCursor(4, 0);
      print2digits(RTC.hours);
      lcd.print(":");
      print2digits(RTC.minutes);
      lcd.print(":");
      print2digits(RTC.seconds);

      // Show Alarm icon if all the wires are set
      if (wirestate1 == HIGH && wirestate2 == HIGH && wirestate3 == HIGH) {
        lcd.setCursor(15, 0);
        lcd.write(byte(0));
      } else {
        lcd.setCursor(15, 0);
        lcd.print(" ");
      }
      
      // Display Day-of-Week in the lower left corner
      lcd.setCursor(0, 1);
      displayDay(RTC.dayofweek);
    
      // Display date in the lower right corner
      lcd.setCursor(5, 1);
      lcd.print(" ");
      print2digits(RTC.dayofmonth);
      lcd.print("/");
      print2digits(RTC.month);
      lcd.print("/");
      lcd.print(RTC.year);
      delay(300);
      break;
    
    // display setting alarm hours
    case 1: 
      // Show the alarm time last set (if the module has been reset this wil default be 00:00:00)
      lcd.setCursor(4, 0);
      print2digits(alarmHour);
      lcd.print(":");
      print2digits(alarmMinutes);
      lcd.print(":00");

      // Notify the user to set the alarm hour
      lcd.setCursor(0,1);
      lcd.print("Set alarm hour");
      delay(200);
      break;

    // Display setting alarm minutes  
    case 2: 
      // Show the alarm time last set
      lcd.setCursor(4, 0);
      print2digits(alarmHour);
      lcd.print(":");
      print2digits(alarmMinutes);
      lcd.print(":00");

      // Notify the user to set the alarm minutes
      lcd.setCursor(0,1);
      lcd.print("Set alarm mins");
      delay(200);
      break;

    // Display alarm fase  
    case 3: 
      // If we are still waiting for the user to wake up display time
      if (alarmCount < 15 && history1 == 0){
        lcd.clear();
        lcd.setCursor(4, 0);
        print2digits(RTC.hours);
        lcd.print(":");
        print2digits(RTC.minutes);
        lcd.print(":");
        print2digits(RTC.seconds);
        lcd.setCursor(0,1);
        
        // Notify wakey wakey
        lcd.print("Wakey, Wakey!");
        delay(250);
        alarmCount += 1;
      }
      // If the first puzzle (wires) is running for 15 seconds display a hint
      if (history1 == 0 && alarmCount >= 15 ){
        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("Cut the correct");
        lcd.setCursor(0,1);
        lcd.print("wire! Fast!");
      }
      // if the secont puzzle (simon says) is running notify a hint
      else if (history2 == 1 && alarmCount >= 15){
        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("Press the buttons");
        lcd.setCursor(0,1);
        lcd.print("in the order");
        delay(100);
      }
      break;

    // Set the year
    case 4: 
      // Notify the user to set the year
      lcd.setCursor(2,0);
      lcd.print("Set the year");
      
     // Display date in the lower right corner
      lcd.setCursor(5, 1);
      lcd.print(" ");
      print2digits(timeDay);
      lcd.print("/");
      print2digits(timeMonth);
      lcd.print("/");
      lcd.print(timeYear);
      delay(100);
      break;

    // Set the Month 
    case 5: 
      // Notify the user to set the month
      lcd.setCursor(1,0);
      lcd.print("Set the month");
      
     // Display date in the lower right corner
      lcd.setCursor(5, 1);
      lcd.print(" ");
      print2digits(timeDay);
      lcd.print("/");
      print2digits(timeMonth);
      lcd.print("/");
      lcd.print(timeYear);
      delay(100);
      break;

    // Set the day
    case 6: 
      // Notify the user to set the day
      lcd.setCursor(2,0);
      lcd.print("Set the day");
      
      // Display date in the lower right corner
      lcd.setCursor(5, 1);
      lcd.print(" ");
      print2digits(timeDay);
      lcd.print("/");
      print2digits(timeMonth);
      lcd.print("/");
      lcd.print(timeYear);
      delay(100);
      break;

    // Set the type of day 
    case 7: 
      // Notify the user to set the type of day
      lcd.setCursor(0,0);
      lcd.print("Set type of day");
      
      // Display Day-of-Week in the lower left corner
      lcd.setCursor(0, 1);
      displayDay(timeTypeOfDay);
      delay(100);
      break;

    // Set the Hours  
    case 8: 
      // Display the latest set time
      lcd.setCursor(4, 0);
      print2digits(timeHour);
      lcd.print(":");
      print2digits(timeMinutes);
      lcd.print(":00");
      
      // Notify the user to set the hour
      lcd.setCursor(4,1);
      lcd.print("Set hours");
      delay(100);
      break;

    // Set the minutes
    case 9:
      // Display the latest set time
      lcd.setCursor(4, 0);
      print2digits(timeHour);
      lcd.print(":");
      print2digits(timeMinutes);
      lcd.print(":00");

      // Notify the user to set the minutes
      lcd.setCursor(3,1);
      lcd.print("Set minutes");
      delay(100);
      break;
  }
}

/**
 * Convert the day-of-the-week to a visible format and display this
 */
void displayDay(int day) {
  String days[8] = {"","Mon","Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
  lcd.setCursor(0,1);
  lcd.print(days[day]);
}

/**
 * If the number is a single digit at a 0 for display reasons
 */
void print2digits(int number) {
  // If larger than 0 and below 10 add a 0 then add the number
  if (number >= 0 && number < 10) {
    lcd.write('0');
  }
  lcd.print(number);
}

