/**
 * Copyright: Group B, 2016
 * Authors: Merijn Freie, Pepijn van Hengel, Raoul Vos
 * 
 * The puzzle module that allows us to play the wire game
 */ 

/**
 * Blinks the led color of the wire that needs to be "cut"
 * The color is decided by the random number generated in the alarm setup
 */
void ledBlink(){
  if (randNumber == 0){
   registerWrite(wire_red, HIGH);
   delay(250);
   registerWrite(wire_red, LOW);
  }
  else if (randNumber == 1){
   registerWrite(wire_yellow, HIGH);
   delay(250);
   registerWrite(wire_yellow, LOW);
  }
  else if (randNumber == 2){
   registerWrite(wire_blue, HIGH);
   delay(250);
   registerWrite(wire_blue, LOW);
  }
}

/**
 * Function to check the wires the same way the buttons are checked
 */
void checkWires(){
  ledBlink();
    // if the correct wire is pulled go to the next puzzle
      if (wirestate1 == LOW){
        if (randNumber == 0){
          int leds[6] = {LOW,LOW,LOW,HIGH,HIGH,HIGH};
          registerWriteArray(leds);
          history1 += 1;
          delay(1000);
        }
        // else notify the user failed
        else {
          lcd.clear();
          lcd.setCursor(0,1);
          lcd.print("Wrong! Try again");
          for(int a = 0; a < 5; a++){
            beep(200);
            ledBlink();
          }
        }
      }

      // if the correct wire is pulled go to the next puzzle
      else if (wirestate2 == LOW){
       if (randNumber == 1){
          int leds[6] = {LOW,LOW,LOW,HIGH,HIGH,HIGH};
          registerWriteArray(leds);
          history1 += 1;
          delay(1000);
        }
        // Notify the user failed
        else {
          lcd.clear();
          lcd.setCursor(0,1);
          lcd.print("Wrong! Try again");
          for(int a = 0; a < 5; a++){
            beep(200);
            ledBlink();
          }
        }
      }

      // if the correct wire is pulled go to the next puzzle
      else if (wirestate3 == LOW){
        if (randNumber == 2){
          int leds[6] = {LOW,LOW,LOW,HIGH,HIGH,HIGH};
          registerWriteArray(leds);
          history1 += 1;
          delay(1000);
        }
        // notify the user failed
        else {
          lcd.clear();
          lcd.setCursor(0,1);
          lcd.print("Wrong! Try again");
          for(int a = 0; a < 5; a++){
            beep(200);
            ledBlink();
          }
        }
      }
      // beep
      beep(500);
}

