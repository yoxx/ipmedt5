/**
 * Copyright: Group B, 2016
 * Authors: Merijn Freie, Pepijn van Hengel, Raoul Vos
 * 
 * The functions to send data to the shift registers
 */

/**
 * Write a single value to a register pin
 * 
 * @var whichPin int - The pin we want to write to
 * @var whichState int - The state we want to give that pin
 */
void registerWrite(int whichPin, int whichState) {
  // the bits you want to send
  byte bitsToSend = 0;
  // turn off the output so the pins don't light up
  // while you're shifting bits:
  digitalWrite(shift_latch, LOW);
  // turn on the next highest bit in bitsToSend:
  bitWrite(bitsToSend, whichPin, whichState);
  // shift the bits out:
  shiftOut(shift_data, shift_clock, MSBFIRST, bitsToSend);
  // turn on the output so the LEDs can light up:
  digitalWrite(shift_latch, HIGH);
}

/**
 * Pass an array of states to the register to set as whole
 * 
 * @var pins int[6] - Array filled with states for the 6 pin in the array known as the keys
 */
void registerWriteArray(int pins[6]){
  // the bits you want to send
  byte bitsToSend = 0;
  // turn off the output so the pins don't light up
  // while you're shifting bits:
  digitalWrite(shift_latch, LOW);
  // Loop trough the array and for each pin (key + 1) set the new state
  for(int a = 0; a < 6; a++) {
    bitWrite(bitsToSend, a+1, pins[a]);
  }
  // shift the bits out:
  shiftOut(shift_data, shift_clock, MSBFIRST, bitsToSend);
  // turn on the output so the LEDs can light up:
  digitalWrite(shift_latch, HIGH);
}

