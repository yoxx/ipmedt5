/**
 * Copyright: Group B, 2016
 * Authors: Merijn Freie, Pepijn van Hengel, Raoul Vos
 * 
 * The time module that allows us to change and set the time and date displayed on the clock
 */

/**
 * The wires are not connected check if the user wants to set the time using the buttons
 */
void checkSetTime() {
  // If the clockstate in alarmsetting mode
  if (clockstate != 0 && clockstate < 4){
    clockstate = 0;
  }
  // If the set button is pressed
  if (buttonstate2 == HIGH && clockstate != 3) {
      lcd.clear();
      //If the clockstate is 0 set to clockstate 4 and recieve the current time
      if (clockstate == 0 ){
        clockstate = 4;
        
        // Retrieve the current time
        timeHour = RTC.hours;
        timeMinutes = RTC.minutes;
        timeTypeOfDay = RTC.dayofweek;
        timeDay = RTC.dayofmonth;
        timeMonth = RTC.month;
        timeYear = RTC.year;
      } 
      // If clockstate is 9 reset the clockstate
      else if (clockstate == 9) {
        // If there was a change to the time set the time
        if (timeHistory != 0) {
          timeHistory = 0;
          setNewTime();
          // Notify the user the time is set
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("Time set!");
          delay(1000);
          lcd.clear();
        }
        // reset the clockstate
        clockstate = 0;
      } else {
        // Go to the next clockstate
        clockstate += 1;
      }
      delay(500);
    } 

    // If the (-) button is pressed
    if (buttonstate1 == HIGH && clockstate > 3 && clockstate != 3) {
      // Set the history +1 to check if the time was changed
      timeHistory += 1;
      // Switch through the clockstates changing the date or time
      switch(clockstate){
        // Set the year -1
        case 4:
          timeYear -= 1;
          break;
        // Set the month -1
        case 5:
          timeMonth -= 1;
          if (timeMonth < 1) {
            timeMonth = 12;
          }
          break;
        // Set the day -1
        case 6:
          timeDay -= 1;
          if (timeDay < 1) {
            timeDay = 31;
          }
          break;
        // Set the day-of-the-week -1
        case 7:
          timeTypeOfDay -= 1;
          if (timeTypeOfDay < 1) {
            timeTypeOfDay = 7;
          }
          break;
        // Set the hour -1
        case 8:
          timeHour -= 1;
          if (timeHour < 0) {
            timeHour = 23;
          }
          break;
        // Set the minutes -1
        case 9:
          timeMinutes -= 1;
          if (timeMinutes < 0) {
            timeMinutes = 59;
          }
          break;
      }
      delay(100);
    } 

    // If the (+) is pressed
    if (buttonstate3 == HIGH && clockstate > 3 && clockstate != 3) {
      // Change the history to show we changed the time or date
      timeHistory += 1;
      // Loop through the switch of clockstates
      switch(clockstate){
        // Set the year +1
        case 4:
          timeYear += 1;
          break;
        // Set the month +1
        case 5:
          timeMonth += 1;
          if (timeMonth > 12) {
            timeMonth = 1;
          }
          break;
        // Set the day +1
        case 6:
          timeDay += 1;
          if (timeDay > 31) {
            timeDay = 1;
          }
          break;
        // Set the day-of-the-week +1
        case 7:
          timeTypeOfDay += 1;
          if (timeTypeOfDay > 7) {
            timeTypeOfDay = 1;
          }
          break;
        // Set the hour +1
        case 8:
          timeHour += 1;
          if (timeHour > 24) {
            timeHour = 0;
          }
          break;
        // Set the minutes +1
        case 9:
          timeMinutes += 1;
          if (timeMinutes > 59) {
            timeMinutes = 0;
          }
          break;
      }
      delay(100);
    }
}

/**
 * This function writes the new time to the RTC
 */
void setNewTime(){
  // Set Date/ Time (seconds, minutes, hours, day of the week, day of the month, month, year)
  RTC.setDS1302Time(00, timeMinutes, timeHour, timeTypeOfDay, timeDay, timeMonth, timeYear);
}

