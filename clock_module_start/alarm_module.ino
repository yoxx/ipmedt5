/**
 * Copyright: Group B, 2016
 * Authors: Merijn Freie, Pepijn van Hengel, Raoul Vos
 * 
 * The alarm module that governs the functions used when setting or checking the alarm
 */


/**
 * Beeps the active buzzer for a certain amount of time this acts as wakeup alarm
 */
void beep(unsigned char delayms){
  analogWrite(9, 222);      // Almost any value can be used except 0 and 255
                           // experiment to get the best tone
  delay(delayms);          // wait for a delayms ms
  analogWrite(9, 0);       // 0 turns it off
  delay(delayms);          // wait for a delayms ms   
} 

/**
 * Check if the alarm time is reached and if the alarm is enabled or disabled
 */
void checkAlarm(){
  // If the alarm hour is reached
  if(alarmHour == RTC.hours) {
    if(alarmMinutes == RTC.minutes){
      if (wirestate1 != HIGH || wirestate2 != HIGH || wirestate3 != HIGH) {
        lcd.clear();
        lcd.setCursor(0,1);
        lcd.print("Alarm disabled");
        delay(2000);
        lcd.clear();
      } else if (alarmstate == 0) {
        if (alarm_setup == 0) {
          // Randomize the wire
          randomSeed(analogRead(0));
          randNumber = random(3);
    
          // Randomize the leds
          for (int i = 0; i < 4; i ++) {
            combination[i] = random(3);
          }
          alarm_setup = 1;
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("Cut the");
          lcd.setCursor(0,1);
          lcd.print("correct wire!");
        }
        clockstate = 3;
      }
    }
  } else { 
    // reset the alarmstate so it will run again the next morning
    alarmstate = 0;
  }
}

/**
 * When the alarmstate is reached run the puzzles
 */
void runAlarm() {
  // Start the wire puzzle
    if (history1 == 0) {
      checkWires();
    }
    // If the wire puzzle is finished
    if (history1 == 1) {
      if (history2 == 0) {
      // Start the leds from simon says
      simonSaysLeds();
      history2 = history2 + 1;
      } else { 
        // Check for button input to solve the simonsays
        checkSimon();
      }
    }
}

/**
 * Check if we should set the alarm time when the buttons are pressed
 */
void checkSetAlarm(){
    // If the clockstate in timesetting mode
    if (clockstate > 3){
        clockstate = 0;
        lcd.clear();
    }
    // If the set button is pressed
    if (buttonstate2 == HIGH && clockstate != 3) {
      // Clear the lcd screen
      lcd.clear();
      // If we reached the final clockstate (clockstate 3 is alarm)
      if (clockstate == 2) {
        // Reset the clockstate to default
        clockstate = 0;
        // Notify the user the alarm is set
        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print("Alarm set!");
        delay(1000);
        lcd.clear();
      } else {
        // Go one clockstate up
        clockstate += 1;
      }
      // Reset the alarmstate so the alarm can go again
      alarmstate = 0;
      delay(500);
    } 

    // If the (-) button is pressed
    if (buttonstate1 == HIGH && clockstate > 0 && clockstate != 3) {
      // Set the hours
      if (clockstate == 1) {
        alarmHour -= 1;
        // Hours cannot go below 0 so if that happens reset to 23
        if (alarmHour < 0) {
          alarmHour = 23;
        }
      }
      // Set the minutes
      if (clockstate == 2) {
        alarmMinutes -= 1;
        // Minutes cannot go below 0 if that happens reset to 59
        if (alarmMinutes < 0) {
          alarmMinutes = 59;
        }
      }
      delay(100);
    } 

    // If the (+) button is pressed
    if (buttonstate3 == HIGH && clockstate > 0 && clockstate != 3) {
      // Set the hours
      if (clockstate == 1) {
        alarmHour += 1;
        // Hours cannot go above 23 if this happens reset to 0
        if (alarmHour > 23) {
          alarmHour = 0;
        }
      }
      // Set the minutes
      if (clockstate == 2) {
        alarmMinutes += 1;
        // Minutes cannot go above 59 if this happens reset to 0
        if (alarmMinutes > 59) {
          alarmMinutes = 0;
        }
      }
      delay(100);
    }
}

