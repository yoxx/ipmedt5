/**
 * Copyright: Group B, 2016
 * Authors: Merijn Freie, Pepijn van Hengel, Raoul Vos
 * 
 * The main module that starts the alarm clock
 */

/**
 * Include Libraries
 */
#include <virtuabotixRTC.h>
#include <LiquidCrystal.h>

/**
 * Define constants
 */
// Define Button pins
const int button1 = 6;
const int button2 = 7;
const int button3 = 8;

// Wire-buttons
const int wire1 = A0;
const int wire2 = A1;
const int wire3 = A2;

// Define Shiftregister pins
const int shift_data = A3;
const int shift_latch = A4;
const int shift_clock = A5;

// Define Light pins
const int simon_red = 1;
const int simon_yellow = 2;
const int simon_blue = 3;
const int wire_red = 4;
const int wire_yellow = 5;
const int wire_blue = 6;

/**
 * Define globals
 */
// ClockState
int clockstate = 0;
int alarmstate = 0;

// Buttonstates
int buttonstate1 = 0;
int buttonstate2 = 0;
int buttonstate3 = 0;

// Wirestates
int wirestate1 = 0;
int wirestate2 = 0;
int wirestate3 = 0;

// Variables for setting time
int timeHour = 0;
int timeMinutes = 0;
int timeTypeOfDay = 1;
int timeDay = 1;
int timeMonth = 1;
int timeYear = 2016;
int timeHistory = 0;

// Variables for setting the alarm time
int alarmHour = 0;
int alarmMinutes = 0;
int alarmCount = 0;

// Alarm icon for displaying a custom icon on the lcd screen
byte wekker[8] = {
    B00100,
    B01110,
    B01110,
    B11111,
    B00100,
    B00000,
    B00000,
};

// Global beepcount for making multiple short beeps
int beepCount = 0;

// Wires history state
int history1 = 0;
// SimonSays history state
int history2 = 0;
// Alarm Setup history state
int alarm_setup = 0;

// Count input and input array
int pwcount;
byte userInput[4];

// Simon Says combination - (Gets randomized)
byte combination[4];

// RandomNumber
int randNumber;

/**
 * Initialize the Libraries
 */
// Init RealTimeClockObject => pins(SCLK -> 12, I/O -> 11, CE -> 10)
virtuabotixRTC RTC(12, 11, 10);

// Initialize the LCD => pins(0,1,2,3,4,5)
LiquidCrystal lcd(0, 1, 2, 3, 4, 5);

/**
 * Setup
 */
void setup() {
  // Start the LCD
  lcd.begin(16, 2);

  // Setup the buttons
  pinMode(button1, INPUT);
  pinMode(button2, INPUT);
  pinMode(button3, INPUT);

  // Setup the wires
  pinMode(wire1, INPUT);
  pinMode(wire2, INPUT);
  pinMode(wire3, INPUT);

  // Setup Shiftregister
  pinMode(shift_data, OUTPUT);
  pinMode(shift_latch, OUTPUT);
  pinMode(shift_clock, OUTPUT);

  // Setup lights
  int setup_leds[6] = {HIGH,HIGH,HIGH,HIGH,HIGH,HIGH};
  registerWriteArray(setup_leds);
  delay(1000);
  int setup_leds2[6] = {LOW,LOW,LOW,LOW,LOW,LOW};
  registerWriteArray(setup_leds2);
  delay(200);

  lcd.createChar(0, wekker);
}

/**
 * Main Loop
 */
void loop() {
  // Read the time from the RTC
  RTC.updateTime();
  // Display the Time and check the Alarmstatus

  // When we are in our normal clockstate (0) then check if the alarm time is reached
  if (clockstate == 0){
    checkAlarm();
  }

  // Show the current time and date or show different times according to the states
  showTime();
  // Check if we are using the buttons
  checkButtons();
}

/**
 * Check buttons for changes
 */
void checkButtons() {
  // Read button input
  buttonstate1 = digitalRead(button1);
  buttonstate2 = digitalRead(button2);
  buttonstate3 = digitalRead(button3);
  // Read wire input
  wirestate1 = digitalRead(wire1);
  wirestate2 = digitalRead(wire2);
  wirestate3 = digitalRead(wire3);
  
  if (clockstate == 3) {
    // If the alarmstate is reached run the alarm
    runAlarm();
  } else {   
    // If all the wires are connected check buttons for setting the alarm time
    if (wirestate1 == HIGH && wirestate2 == HIGH && wirestate3 == HIGH) {
      checkSetAlarm();
    } else { 
      // Set the clock time
      checkSetTime();
    }
  }
}



