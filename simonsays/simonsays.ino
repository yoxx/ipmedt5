const int greenled = 11;
const int redled = 8;
const int blueled = 13;
const int b1 = 2;
const int b2 = 3;
const int b3 = 4;


// check of de reeks goed is doorlopen
int geschiedenis = 0;

// tel input en zet in array
int pwcount;
byte userInput[4];

// de juiste combinatie
byte combination[] = "3212";

// zet de buttons naar 0 in de beginstatus
int buttonstate1 = 0;
int buttonstate2 = 0;
int buttonstate3 = 0;

void setup() {
  // ledjes als output
  pinMode(greenled, OUTPUT);
  pinMode(redled, OUTPUT);
  pinMode(blueled, OUTPUT);

  // knoppen als input
  pinMode(b1, INPUT);
  pinMode(b2, INPUT);
  pinMode(b3, INPUT);

  //serial print
  Serial.begin(9600);
}

void simonSaysLeds(){
  for (byte i = 0; i < 4; i++){
    Serial.print(combination[i]);
    if (combination[i] == 49){
        Serial.print("LED1");
        digitalWrite(greenled, LOW);
        digitalWrite(redled, LOW);
        digitalWrite(blueled, HIGH);
        delay(1000);
        digitalWrite(greenled, LOW);
        digitalWrite(redled, LOW);
        digitalWrite(blueled, LOW);
        delay(500);
    }
    else if (combination[i] == 50){
        Serial.print("LED2");
        digitalWrite(greenled, HIGH);
        digitalWrite(redled, LOW);
        digitalWrite(blueled, LOW);
        delay(1000);
        digitalWrite(greenled, LOW);
        digitalWrite(redled, LOW);
        digitalWrite(blueled, LOW);
        delay(500);
    }
    else if (combination[i] == 51){
        Serial.print("LED3");
        digitalWrite(greenled, LOW);
        digitalWrite(redled, HIGH);
        digitalWrite(blueled, LOW);
        delay(1000);
        digitalWrite(greenled, LOW);
        digitalWrite(redled, LOW);
        digitalWrite(blueled, LOW);
        delay(500);
    }
    
  }
}

void error(){
  digitalWrite(greenled, HIGH);
  digitalWrite(redled, HIGH);
  digitalWrite(blueled, HIGH);
  delay(200);
  digitalWrite(greenled, LOW);
  digitalWrite(redled, LOW);
  digitalWrite(blueled, LOW);
  delay(200);
} 

void victory(){
  digitalWrite(greenled, LOW);
  digitalWrite(redled, HIGH);
  digitalWrite(blueled, LOW);
  delay(100);
  digitalWrite(greenled, HIGH);
  digitalWrite(redled, LOW);
  digitalWrite(blueled, LOW);
  delay(100);
  digitalWrite(greenled, LOW);
  digitalWrite(redled, LOW);
  digitalWrite(blueled, HIGH);
  delay(100);
  digitalWrite(greenled, HIGH);
  digitalWrite(redled, LOW);
  digitalWrite(blueled, LOW);
  delay(100);
  digitalWrite(greenled, LOW);
  digitalWrite(redled, LOW);
  digitalWrite(blueled, LOW);
} 

void loop(){
  buttonstate1 = digitalRead(b1);
  buttonstate2 = digitalRead(b2);
  buttonstate3 = digitalRead(b3);

  if (geschiedenis == 0){
    simonSaysLeds();
    geschiedenis = geschiedenis + 1;
  }
  

  if (buttonstate1 == HIGH){
    userInput[pwcount] = '1';
    pwcount++;
    digitalWrite(blueled, HIGH);
    delay(300);
    digitalWrite(blueled, LOW);
    Serial.print('1');
  }
  if (buttonstate2 == HIGH){
    userInput[pwcount] = '2';
    pwcount++;
    digitalWrite(greenled, HIGH);
    delay(300);
    digitalWrite(greenled, LOW);
    Serial.print('2');
  }
  if (buttonstate3 == HIGH){
    userInput[pwcount] = '3';
    pwcount++;
    digitalWrite(redled, HIGH);
    delay(300);
    digitalWrite(redled, LOW);
    Serial.print('3');
  }

  if (pwcount == 4){
  boolean check = true;
  for(byte n = 0; n < 4; n++){
      if(userInput[n] != combination[n]){
        check = false;
      }
  }

  if (check){
      victory();
      victory();
      Serial.println("Unlocked");
      pwcount = 0;
  } else {
        error();
        error();
        error();
        error();
        geschiedenis = 0;
        Serial.println("Denied");
        pwcount = 0;
  }
  }
}
